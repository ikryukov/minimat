# MINIMAT

A tiny matrix library.

`MINIMAT` is a tiny header-only matrix library, meant to facilitate storage and usage of matrices across library.
It implements a minimal set of features for storage of vectors and matrices.

Class `NumericVector` implements an owning NumericVector, which is responsible for cleaning up its storage. The `NumericVector` implements a general strided NumericVector.

Class `NumericVectorView` is a non-owning NumericVector, which refers to storage of a different NumericVector. It is not responsible for cleaning up.

Class `DenseMatrix` implements a contiguously-stored dense matrix. It is responsible for cleaning up its storage. Accessors `row(i)` and `col(i)` return `VectorViews`, with proper strides.
