#include <cstdlib>
#include <cassert>

#include <iostream>

using namespace std;

#include "NumericVector.hpp"
#include "NumericMatrix.hpp"

int main(int argc, char const *argv[])
{
	NumericVector<double> d = NumericVector<double>::identity(3);
	// NumericVector<double> d(3, 0);

	cout << d << endl;

	NumericVectorView<double> v(d);

	v(1) = 1;

	cout << d << endl;

	assert(d == v); // NumericVector should always be equal to its view

	NumericMatrix<double> A(3, 3, 1);
	A(0,1) = 0;
	cout << A << endl;

	cout << A.row(0) << endl;

	cout << A.col(1) << endl;

	A.row(0)(0) = 3;
	A.col(1)(1) = 5;

	cout << A << endl;

	cout << A.row(0) << "; sum = " << A.row(0).sum() << endl;
	assert(A.row(0).sum() == 4);

	cout << A.col(1) << "; sum = " << A.col(1).sum() << endl;
	assert(A.col(1).sum() == 6);

	cout << "--" << endl;

	A.col(1) = A.col(0);

	cout << A << endl;

	A.print_debug();

	return 0;
}

